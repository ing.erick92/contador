<?php
/**
* Plugin Name: test-plugin
* Plugin URI: https://www.erick.com/
* Description: Change Styles
* Version: 0.1
* Author: Erick Lopez
* Author URI: https://www.erick.com/
**/

$categories = array('Nacional', 'Entretenimiento', 'Tecnología', 'Mascotas', 'Deportes');
if(is_category($categories)){
	//this page belongs to one of these categories
	$custom_css = ''
	
	if(is_category($categories[0])){
		$custom_css = '.page-title { color: #FFFFFF; } .conteiner-title { background-color : #00B049}'
	}
	else if(is_category($categories[1])){
		$custom_css = '.page-title { color: #FFFFFF; } .conteiner-title { background-color : #FFC915}'
	}
	else if(is_category($categories[2])){
		$custom_css = '.page-title { color: #FFFFFF; } .conteiner-title { background-color : #00D3F8}'
	}
	else if(is_category($categories[3])){
		$custom_css = '.page-title { color: #FFFFFF; } .conteiner-title { background-color : #90456D}'
	}
	else if(is_category($categories[4])){
		$custom_css = '.page-title { color: #FFFFFF; } .conteiner-title { background-color : #FF372C}'
	}

	add_action( 'wp_head', 'add_styles' );

	function add_styles(){ ?>
		<style> <?php
			$custom_css
		?></style><?php
	}
}
